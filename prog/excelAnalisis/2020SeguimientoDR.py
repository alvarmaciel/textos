#!/usr/bin/env python3
import pandas as pd

file = "2020SeguimientoDR.xlsx"

data = pd.ExcelFile(file)
print(data.sheet_names)  # esto  devuelve las claves o paginas del excel

df = data.parse(
    "Registro Anahi"
)  # El parse convirte el crudo de los datos en un dataframe.
df.info
print(df.head(10))
