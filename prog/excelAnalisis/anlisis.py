#!/usr/bin/env python3
import os
import pandas as pd
import openpyxl
import pprint

# os.getcwd()
# os.chdir in os.supports_fd
# os.chdir("/home/alvarito/git/textos/prog/excelAnalisis")

file = "produceSales.xlsx"
# data = pd.read_excel(
#     file, None
# )  # el sgundo argumento hace que pandas tome todas las hojas de libro
data = pd.ExcelFile(file)
print(data.sheet_names)  # esto  devuelve las claves o paginas del excel

df = data.parse("Sheet1")  # El parse convirte el crudo de los datos en un dataframe.
df.info
df.head(10)
## Leer en la hoja de cálculo

ps = openpyxl.load_workbook("produceSales.xlsx")

sheet = ps["Sheet1"]
TotalInfo = {}
print(sheet.max_row)

## Usamos un bucle for para iterar entre todas las columnas

for row in range(2, sheet.max_row + 1):
    # cada fila en la planilla representa información de una compra en particular
    produce = sheet[
        "B" + str(row)
    ].value  # toma el el valor de la celda en este caso vegetal
    costo_por_libra = sheet[
        "C" + str(row)
    ].value  # Toma el valor de la celda, en este caso el costo por libra
    libras_vendidas = sheet["D" + str(row)].value  # Tma cuantas libras se vendió
    ventas_totales = sheet["E" + str(row)].value
    # cada valor en una celda es representado por la letra de una columna y un número de filaa.
    # El pimer elemnto en la planilla es B1, el siguiente C1 y así sucsivamnte. Esto permite iterar por todas las celdas

    ## Creamos un diccionario vacío que contendrá toda la información de cada fruta. Usamos el método set.default() para llenar el primer elementoen el diccionario. el primer argumento de set.default() chequea si existe esa clave en el diccionario. si no etá lo reemplaacon el segundo argumento. Asi podemos ir llenando el diccionario con el segundo argumento de la unción set.default
    TotalInfo.setdefault(
        produce,
        {
            "Costo_total_por_libra": 0,
            "Total_libras_vendidas": 0,
            "Total_ventas": 0,
            "Total_instancias_vendidas": 0,
        },
    )
    ## Aspi llenamos el diccionario  0. cuando empieza a iterar lo va llenando
    ## Finalmente llenamos el diccionario, para cada nueva "produce" de cada fila incrementamos la métrica al correspondiente valor en de la nueva fial
    ## Cada fila representa una fruta, así que se incremena por el valor correspondiente
    TotalInfo[produce]["Costo_total_por_libra"] += float(costo_por_libra)
    TotalInfo[produce]["Total_libras_vendidas"] += int(libras_vendidas)
    TotalInfo[produce]["Total_ventas"] += int(ventas_totales)
    # Cada fila representa una fruta , así que incrementamos por 1
    TotalInfo[produce]["Total_instancias_vendidas"] += 1


print("Manzanas = ", TotalInfo["Apples"])

## Escribir los datos a un archivo

resultFile = open("Total_info.txt", "w")
resultFile.write(pprint.pformat(TotalInfo))
resultFile.close()
print("Listo")
