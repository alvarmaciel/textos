#+TITLE: Think Python
* Capítulo 4 Caso de estudio diseño de interfaces
** El módulo tutle
Para chequear si tenemos
turtle instalado
#+begin_src python :results output
import turtle
bob = turtle.Turtle()
#+end_src
#+BEGIN_SRC emacs-lisp
(+ 1 2 3)

#+END_SRC

#+RESULTS:
: 6
