### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 6e23d106-fee3-45a5-acb8-33406b506610
using PlutoUI

# ╔═╡ 0184056c-9c46-11eb-3867-7fa9298c3294
md"Evalúa la siguente expresión:
- ``\large \frac{12}{j} \normalsize \textrm{ cuando } j = 4``
 
Editá el resultado de respuesta para responder"

# ╔═╡ 42e66720-3294-4617-83fe-338a7cab8656
respuesta= 3

# ╔═╡ 631d0cc5-91ee-4d19-9868-16a782084f2f
md"Evalúa ``8 + 3e`` cuando e = 2"

# ╔═╡ 8ad9c907-1fca-4771-b469-c939d012a244
md"## Evaluar expresiones con paréntisis"

# ╔═╡ 652386b2-243e-4926-9f6e-e2ab723ddae2
md"Patrick trató de evaluar la expresión ``7×[8−3×2]`` Su trabajo está a continuación.
- Paso 1 = ``7\times [5\times2]``
- Paso 2 = ``7 \times 10``
- Paso 3 = ``70``
"

# ╔═╡ 2b428adf-264b-4041-8db1-ad188395e1a9
md"- Paso 1 $(@bind eval_exp_r1 CheckBox(default=false))
- Paso 2 $(@bind eval_exp_r2 CheckBox(default=false))
- Paso 3 $(@bind eval_exp_r3 CheckBox(default=false))"

# ╔═╡ ef984b71-e087-4d12-a9d2-25699d1ff49c
md"**Consigna Original**: ¡Donde podemos colocar el paréntesis en ``60 \div 10 \times3`` para hacerla equivalente a ``2``

Consigna nueva: hacé que la variable ```respuesta_2``` sea igual a 2, solo colocando los paréntesis adecuados"

# ╔═╡ 080afef3-647e-493f-b152-4b048193e48c
respuesta_2 = 60 / 10 * 3

# ╔═╡ e9fcb9aa-2fad-405f-a365-569cc6d1aeab
hint(text) = Markdown.MD(Markdown.Admonition("pista", "Pista", [text]));

# ╔═╡ 433abba1-6fc0-49dc-abc7-d8b744d6b08d
hint(md"Editá la celda rosa donde dice respuesta")

# ╔═╡ 6161f2e2-be52-4a92-a05c-e24b4044a762
almost(text) = Markdown.MD(Markdown.Admonition("warning", "Almost there!", [text]));

# ╔═╡ eef69c74-6e8f-41c6-a647-033fab4f7bf7
correct(text=md"¡Genial! Es la respuesta correcta. Vamos ahora a la siguente sección") = Markdown.MD(Markdown.Admonition("correct", "¡Muy Bien!", [text]));

# ╔═╡ b68a7901-be8d-464b-a560-7e1fccaf7259
keep_working(text=md"La respuesta no es la correcta aun.") = Markdown.MD(Markdown.Admonition("danger", "¡A seguir trabjando!", [text]));

# ╔═╡ 750e64a3-36fc-46fd-bb31-fe99030daa42
if respuesta == 3
	correct()
	
else
	keep_working()
end

# ╔═╡ 2ec502d4-c485-4a00-8bbe-e51f0866bfab
if eval_exp_r1 == true
	correct()
	
else
	keep_working()
end

# ╔═╡ dbb0d390-5fb2-42a5-b2d4-2933017c088b
if respuesta_2 == 2
	correct()
	
else
	keep_working()
end

# ╔═╡ 59f6f315-a1f8-4aa8-917c-669592b09b63
md"!!! note

    This is the content of the note."

# ╔═╡ 548ea32d-aaf8-4f7d-a15d-7e0824fb1ca9
md"!!! warning 

    And this is another one."

# ╔═╡ da4b6ad1-2347-42fd-aa4e-e8cfdd57ed2c
md "!!! alert 
    asdsa"

# ╔═╡ Cell order:
# ╠═6e23d106-fee3-45a5-acb8-33406b506610
# ╟─0184056c-9c46-11eb-3867-7fa9298c3294
# ╠═433abba1-6fc0-49dc-abc7-d8b744d6b08d
# ╠═42e66720-3294-4617-83fe-338a7cab8656
# ╠═750e64a3-36fc-46fd-bb31-fe99030daa42
# ╟─631d0cc5-91ee-4d19-9868-16a782084f2f
# ╟─8ad9c907-1fca-4771-b469-c939d012a244
# ╟─652386b2-243e-4926-9f6e-e2ab723ddae2
# ╟─2b428adf-264b-4041-8db1-ad188395e1a9
# ╠═2ec502d4-c485-4a00-8bbe-e51f0866bfab
# ╟─ef984b71-e087-4d12-a9d2-25699d1ff49c
# ╠═080afef3-647e-493f-b152-4b048193e48c
# ╠═dbb0d390-5fb2-42a5-b2d4-2933017c088b
# ╠═e9fcb9aa-2fad-405f-a365-569cc6d1aeab
# ╠═6161f2e2-be52-4a92-a05c-e24b4044a762
# ╠═eef69c74-6e8f-41c6-a647-033fab4f7bf7
# ╠═b68a7901-be8d-464b-a560-7e1fccaf7259
# ╠═59f6f315-a1f8-4aa8-917c-669592b09b63
# ╠═548ea32d-aaf8-4f7d-a15d-7e0824fb1ca9
# ╠═da4b6ad1-2347-42fd-aa4e-e8cfdd57ed2c
