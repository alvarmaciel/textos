### A Pluto.jl notebook ###
# v0.11.4

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ d128b260-d914-11ea-09c4-cfa6b1441505
md" ## Primer ejercicio
- El volumen de una esfera con radio $r$ es $\frac{4}{3}$ π $r^3$. ¿Cuál es el volumen de una esfera con radio 5?"

# ╔═╡ 11ab32a0-d91e-11ea-29c0-d17e7d29d4af
md">mové el slider para ver el valor del volumen desde 0 a 5"

# ╔═╡ 8015b020-d91f-11ea-21c9-31492371d758
md"### Versión para enseñar
- El volumen de una esfera con radio $r$ es $\frac{4}{3}$ π $r^3$. ¿Cuál es el volumen de una esfera con radio 5?
  1. Nosotros creamos la variable `radio` con valor 5. Vos crea debajo de esta celda la variable `volumen` con la expresión matemática que resuelva el problema"

# ╔═╡ 29b8e1d0-d91e-11ea-2541-af368c3e48df
md"## Segundo ejercico

- Supongamos que el precio de venta de un libro es de $ 24.95, pero las librerías obtienen un descuento del 40%. El envío cuesta $3 por la primera copia y 75 centavos por cada copia adicional. ¿Cuál es el costo total al por mayor de 60 copias?"

# ╔═╡ 749b06d0-d91c-11ea-1081-75e1cb860954
@bind r html"<input type=range  min=0 max=5 step=1>"

# ╔═╡ 10e4f1e0-d91d-11ea-0f90-ad8296265894
md"""radio=$r"""

# ╔═╡ 087ca6c0-d91c-11ea-3c42-d512de530dd3
begin
	v= (4 * pi * (r^3))/3
	md"Volumen = $v"
end

# ╔═╡ 64f8a8a0-d920-11ea-1847-a39f2b80ca10
radio=5

# ╔═╡ 4c191130-d925-11ea-10a6-296b7d34c78a
volumen = ((4*pi) * (radio^3))/3

# ╔═╡ 69c5c110-d920-11ea-1084-0faf72693daa
if !@isdefined volumen
	md""" Bueno, veamos esto, para agregar una celda tienen que hacere click en el ➕ que está a la derecha. Una vez que crees la variable asignale el valor o la formula para calcular el volumen de una esfera de radio 5.

	y por ahora no te proecupes por el error que se acá abajo ⏬
	"""
else
	md" ¡Muy Bien! definiste la variable"

end

# ╔═╡ 8f265c00-d923-11ea-165c-d7bbf6ecc401
if volumen == (4 * pi * (radio^3))/3
	md"""#### Excelente!
	![](https://media.giphy.com/media/vMnuZGHJfFSTe/giphy.gif)"""
else
	md"""#### Probá de nuevo

	![](https://media.giphy.com/media/W1f2ZkNPv3m8P60xvd/giphy.gif)"""
end

# ╔═╡ Cell order:
# ╟─d128b260-d914-11ea-09c4-cfa6b1441505
# ╟─11ab32a0-d91e-11ea-29c0-d17e7d29d4af
# ╟─749b06d0-d91c-11ea-1081-75e1cb860954
# ╟─10e4f1e0-d91d-11ea-0f90-ad8296265894
# ╟─087ca6c0-d91c-11ea-3c42-d512de530dd3
# ╟─8015b020-d91f-11ea-21c9-31492371d758
# ╠═64f8a8a0-d920-11ea-1847-a39f2b80ca10
# ╟─69c5c110-d920-11ea-1084-0faf72693daa
# ╠═4c191130-d925-11ea-10a6-296b7d34c78a
# ╟─8f265c00-d923-11ea-165c-d7bbf6ecc401
# ╟─29b8e1d0-d91e-11ea-2541-af368c3e48df
