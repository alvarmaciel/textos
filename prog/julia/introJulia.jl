### A Pluto.jl notebook ###
# v0.11.3

using Markdown
using InteractiveUtils

# ╔═╡ 9f8e5d30-d69a-11ea-3bf8-c7d2035ba9b9
md"## Un nuevo incio 🚀

Notas y ejercicios de [Introducción a la Programación en Julia](https://introajulia.org/) "

# ╔═╡ 5781b054-d69b-11ea-3bc4-f55bed4cee96
md"## El camino de la programación

Al usar [Pluto](https://github.com/fonsp/Pluto.jl) como espacio de toma de notas, el clásico:

`println('hola mundo')`
 
se escribe en un bloque `begin` - `end` en el cual a una variable se le asigna un valor y luego simplemete la llama para que ejecute su salida.
Entonces, vamos con un clásico..."

# ╔═╡ edbcc542-d6a8-11ea-17c0-0ffc8ed5a374
-2

# ╔═╡ f84b36b0-d6a5-11ea-1da7-df6eaee110a7
md"### Ejercicios

El clasico DeBugueo 🐞"

# ╔═╡ d8da1c2c-d6a5-11ea-1e79-5bc1d9819096
# Omision de comillas
begin
	hola = "Hola Mundo 🌎
	hola
end

# ╔═╡ 1689640a-d6a9-11ea-112f-bbcd0f1a9e35
02

# ╔═╡ 1c015a26-d6a9-11ea-2e7f-b1e33d110ef0
3 2

# ╔═╡ 1dec9c96-d6aa-11ea-2b2c-5df0ccd43e32
md" 1. Cuantos segundos hay en 4 minuttos y 42 segundso"

# ╔═╡ e5932004-d6a9-11ea-1fe4-136d981cdfab
md"2. ¿Cuántas millas hay en 10 kilómetros?

>Hay 1,61 kilómetros en una milla.
"

# ╔═╡ 50e9e4ea-d6ab-11ea-273c-411384e998a0
md"3. Si corres una carrera de 10 kilómetros en 37 minutos y 48 segundos, ¿cuál es tu ritmo promedio (tiempo por milla en minutos y segundos)? ¿Cuál es tu velocidad promedio en millas por hora?"

# ╔═╡ 1e3a2908-d820-11ea-3d08-fb85f8fd7c59
md"## Variables, expresiones y sentencias


"

# ╔═╡ d78a2038-d826-11ea-03c1-1d58a3d52018
5
x = 5
x + 1

# ╔═╡ d8f95362-d826-11ea-01d4-47777ecca962


# ╔═╡ 4683c780-d6a5-11ea-021e-ad3b7b2594f9
begin
	hola = "Hola Mundo 🌎"
	hola
end

# ╔═╡ ac0f21b2-d6a8-11ea-0b0d-0115c09fc8ce
2+2

# ╔═╡ f10ad4a0-d6a8-11ea-35e0-3f4bedd36c57
3*4

# ╔═╡ f596db72-d6a8-11ea-2a28-413d3c342775
42/7

# ╔═╡ a2557f86-d6a8-11ea-3814-67fa36eeef2a
# Signos negativos y positivos
2++2

# ╔═╡ 200ce432-d6a9-11ea-3d93-9d86fea5a935
begin
	minutos=42
	min_a_seg=minutos * 60
	totalseg = min_a_seg + 42
	totalseg
end

# ╔═╡ 346df85a-d6aa-11ea-0af7-57cead709f2c
begin
	kilometros=10
	mill_km = kilometros*1,61
	mill_km
end

# ╔═╡ 5efac64e-d6ab-11ea-2188-1b8cd5044228
begin
	min2=37
	min_a_seg2=min2*60
	totalseg2=min_a_seg2 + 48
	ritmo=totalseg2/kilometros
	min,seg=divrem(ritmo,60)
	total_carrera = "El ritmo es $min con $seg segundos"
end

# ╔═╡ Cell order:
# ╟─9f8e5d30-d69a-11ea-3bf8-c7d2035ba9b9
# ╟─5781b054-d69b-11ea-3bc4-f55bed4cee96
# ╠═4683c780-d6a5-11ea-021e-ad3b7b2594f9
# ╠═ac0f21b2-d6a8-11ea-0b0d-0115c09fc8ce
# ╠═edbcc542-d6a8-11ea-17c0-0ffc8ed5a374
# ╠═f10ad4a0-d6a8-11ea-35e0-3f4bedd36c57
# ╠═f596db72-d6a8-11ea-2a28-413d3c342775
# ╟─f84b36b0-d6a5-11ea-1da7-df6eaee110a7
# ╠═d8da1c2c-d6a5-11ea-1e79-5bc1d9819096
# ╠═a2557f86-d6a8-11ea-3814-67fa36eeef2a
# ╠═1689640a-d6a9-11ea-112f-bbcd0f1a9e35
# ╠═1c015a26-d6a9-11ea-2e7f-b1e33d110ef0
# ╟─1dec9c96-d6aa-11ea-2b2c-5df0ccd43e32
# ╠═200ce432-d6a9-11ea-3d93-9d86fea5a935
# ╟─e5932004-d6a9-11ea-1fe4-136d981cdfab
# ╠═346df85a-d6aa-11ea-0af7-57cead709f2c
# ╟─50e9e4ea-d6ab-11ea-273c-411384e998a0
# ╠═5efac64e-d6ab-11ea-2188-1b8cd5044228
# ╟─1e3a2908-d820-11ea-3d08-fb85f8fd7c59
# ╠═d78a2038-d826-11ea-03c1-1d58a3d52018
# ╠═d8f95362-d826-11ea-01d4-47777ecca962
