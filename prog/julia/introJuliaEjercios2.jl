### A Pluto.jl notebook ###
# v0.11.4

using Markdown
using InteractiveUtils

# ╔═╡ 8015b020-d91f-11ea-21c9-31492371d758
md"# Versión de Notebook para enseñar Julia
Esta notebook es un prueba para presentar los ejercicios del libro [Intro Julia](https://introajulia.org/#_ejercicios_2) para generar instancias reactivas e interactivas."


# ╔═╡ 7b6e7590-d927-11ea-2e61-232000cbdd8a
md"""
### Primer ejercicio capítulo 2 variables

- El volumen de una esfera con radio $r$ es $\frac{4}{3}$ π $r^3$. ¿Cuál es el volumen de una esfera con radio 5?
  1. les creamos la variabel `radio` con valor 5. ustedes creen debajo  `volumen` con la expresión matemática que resuelva el problema"""

# ╔═╡ 29b8e1d0-d91e-11ea-2541-af368c3e48df
md"## Segundo ejercico

- Supongamos que el precio de venta de un libro es de $ 24.95, pero las librerías obtienen un descuento del 40%. El envío cuesta $3 por la primera copia y 75 centavos por cada copia adicional. ¿Cuál es el costo total al por mayor de 60 copias?"

# ╔═╡ 64f8a8a0-d920-11ea-1847-a39f2b80ca10
radio=5

# ╔═╡ 69c5c110-d920-11ea-1084-0faf72693daa
if !@isdefined volumen
	md""" Bueno, veamos esto, para agregar una celda tienen que hacere click en el ➕ que está a la derecha. Una vez que crees la variable asignale el valor o la formula para calcular el volumen de una esfera de radio 5.

	y por ahora no se proecupen por el error que se acá abajo ⏬ va a cambiar a medida que resuelvan la actividad
	"""
else
	md" ¡Muy Bien! definiste la variable"

end

# ╔═╡ 8f265c00-d923-11ea-165c-d7bbf6ecc401
if volumen == (4 * pi * (radio^3))/3
	md"""#### Excelente!
	![](https://media.giphy.com/media/vMnuZGHJfFSTe/giphy.gif)"""
else
	md"""#### Probá de nuevo

	![](https://media.giphy.com/media/W1f2ZkNPv3m8P60xvd/giphy.gif)"""
end

# ╔═╡ Cell order:
# ╟─8015b020-d91f-11ea-21c9-31492371d758
# ╟─7b6e7590-d927-11ea-2e61-232000cbdd8a
# ╟─64f8a8a0-d920-11ea-1847-a39f2b80ca10
# ╟─69c5c110-d920-11ea-1084-0faf72693daa
# ╟─8f265c00-d923-11ea-165c-d7bbf6ecc401
# ╟─29b8e1d0-d91e-11ea-2541-af368c3e48df
