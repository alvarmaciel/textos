def majority_element_indexes(lst):
    """
    Return a list of the indexes of the majority element.
    Majority element is the element that appears more than
    floor(n / 2) times.
    If there is no majority element, return []
    >>> majority_element_indexes([1, 1, 2])
    [0, 1]
    >>> majority_element_indexes([1, 1, 2, 3, 4])
    []
    >>> majority_element_indexes([1, 2])
    []
    >>> majority_element_indexes([1])
    [0]
    """
    # Encontrar el elemnto mayoritario
    # Si no hay imprimer una lista vacía
    # Si hay elementos mayoritariso imprimer sus índices
    from collections import Counter

    if lst == []:
        return []

    count = Counter(lst)
    top_elements = sorted(count.keys(), key=lambda x: -count[x])
    majory_element = top_elements[0]

    if count[majory_element] > len(lst) // 2:
        return [i for i, elem in enumerate(lst) if elem == majory_element]
    else:
        return []
