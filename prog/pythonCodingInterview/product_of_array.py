def product_of_all_numbers_excepts_itself(numbers):
    """
    Given an array of numbers, replace each number with the product of all the
    numbers in the array except the number itself *without* using division.
    >>> numbers = [1,2,3,4]
    >>> product_of_all_numbers_excepts_itself(numbers)
    [24, 12, 8, 6]
    >>> product_of_all_numbers_excepts_itself([2, 2, 2])
    [4, 4, 4]
    """

    list_result = []
    numbers_enumerate = enumerate(numbers)
    for i, _ in numbers_enumerate:
        results = 1

        for indx, num in numbers_enumerate:
            if i != indx:
                results *= num
        list_result.append(results)

    return list_result


# list_result = [results := results * num for num in numbers if num != numbers[i]]
