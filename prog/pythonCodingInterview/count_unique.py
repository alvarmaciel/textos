def count_unique(s):
    """
    Count number fo unique characteres in s
    >>> count_unique('aabb')
    2
    >>> count_unique("abcdef")
    6
    """
    # list resolution
    # unique_char = []
    # for char in s:
    #     if char not in unique_char:
    #         unique_char.append(char)
    # return len(unique_char)

    # Set() resolution
    # unique_char = set()
    # for char in s:
    #     if char not in unique_char:
    #         unique_char.add(char)
    # return len(unique_char)

    # Set comprehension
    # return len({char for char in s})

    # Set conversion
    return len(set(s))
