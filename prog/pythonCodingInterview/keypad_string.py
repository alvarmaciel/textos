import string


def create_maping_key():
    """
    Create de maping of key to letters
    """
    possible_letters = string.ascii_letters
    possible_keys = string.digits
    map_dic = {}
    start_index = 0
    for key in possible_keys:
        if key == "0":
            map_dic[key] = " "
        elif key == "1":
            map_dic[key] = ""
        else:
            num_letter = 3
            if key in {"7", "9"}:
                num_letter = 4
            letters = possible_letters[start_index : start_index + num_letter]
            map_dic[key] = letters
            start_index += num_letter
    return map_dic


def keypad_string(keys):
    """
    Given a string consisting of 0-9,
    find the string that is created using
    a standard phone keypad
    | 1        | 2 (abc) | 3 (def)  |
    | 4 (ghi)  | 5 (jkl) | 6 (mno)  |
    | 7 (pqrs) | 8 (tuv) | 9 (wxyz) |
    |     *    | 0 ( )   |     #    |
    You can ignore 1, and 0 corresponds to space
    >>> keypad_string("4433555555666")
    'hello'
    >>> keypad_string("2022")
    'a b'
    >>> keypad_string("")
    ''
    >>> keypad_string("111")
    ''
    >>> keypad_string("2222")
    'ca'
    >>> keypad_string("12345")
    'adgj'
    """
    map_dic = create_maping_key()
    # map_dic = {
    #     "1": "",
    #     "2": "abc",
    #     "3": "def",
    #     "4": "ghi",
    #     "5": "jkl",
    #     "6": "mno",
    #     "7": "pqrs",
    #     "8": "tuv",
    #     "9": "wxyz",
    #     "0": " ",
    #     "*": "",
    #     "#": "",
    # }

    def return_letter_3(key_pressed, i):
        if keys.count(key_pressed, i, i + 3) == 3:
            letter = map_dic[key_pressed][2:3]
            i += 3
        elif keys.count(key_pressed, i, i + 2) == 2:
            letter = map_dic[key_pressed][1:2]
            i += 2
        elif keys.count(key_pressed, i, i + 1) == 1:
            letter = map_dic[key_pressed][0:1]
            i += 1
        return letter, i

    def return_letter_4(key_pressed, i):
        if keys.count(key_pressed, i, i + 4) == 4:
            letter = map_dic[key_pressed][3:4]
            i += 4
        elif keys.count(key_pressed, i, i + 3) == 3:
            letter = map_dic[key_pressed][2:3]
            i += 3
        elif keys.count(key_pressed, i, i + 2) == 2:
            letter = map_dic[key_pressed][1:2]
            i += 2
        elif keys.count(key_pressed, i, i + 1) == 1:
            letter = map_dic[key_pressed][0:1]
            i += 1
        return letter, i

    decoded_string = ""
    letter = ""
    i = 0
    while i != len(keys):
        # breakpoint()
        if keys[i] == "2":
            letter, i = return_letter_3(keys[i], i)
        elif keys[i] == "3":
            letter, i = return_letter_3(keys[i], i)
        elif keys[i] == "4":
            letter, i = return_letter_3(keys[i], i)
        elif keys[i] == "5":
            letter, i = return_letter_3(keys[i], i)
        elif keys[i] == "6":
            letter, i = return_letter_3(keys[i], i)
        elif keys[i] == "8":
            letter, i = return_letter_3(keys[i], i)
        elif keys[i] == "7":
            letter, i = return_letter_4(keys[i], i)
        elif keys[i] == "8":
            letter, i = return_letter_4(keys[i], i)
        elif keys[i] == "0":
            letter = " "
            i += 1
        else:
            i += 1

        decoded_string += letter

    return decoded_string
