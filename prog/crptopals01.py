#!/usr/bin/env python3
import base64

"""
Convert hex to base64

The string:

49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d

Should produce:

SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t

So go ahead and make that happen. You'll need to use this code for the rest of the exercises.
Cryptopals Rule

Always operate on raw bytes, never on encoded strings. Only use hex and base64 for pretty-printing.
"""


def from_hex_to_base64(string_hex):
    bytes_data = bytes.fromhex(string_hex)
    base64_code = base64.b64encode(bytes_data)
    return base64_code


if __name__ == "__main__":
    hex_data = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
    print(from_hex_to_base64(hex_data))
g
