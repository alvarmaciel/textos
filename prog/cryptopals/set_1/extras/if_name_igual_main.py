'''
Python if__name__==__main__ explained 
https://www.freecodecamp.org/news/if-name-main-python-example/
cuando el interprete corre un módulo, la variable __name__ se define como __main__ si el módulo que está corriendo es el programa principal.
Pero si el código está importando el modilo de otro módulo, entonces la variable __name__ se definirá a ese módulo
'''
