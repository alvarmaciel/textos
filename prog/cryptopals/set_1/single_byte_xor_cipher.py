import hex_to_x as hx
import fixed_XOR as fx
import binascii
import sys
from binascii import unhexlify

"""
 The hex encoded string:

1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736

... has been XOR'd against a single character. Find the key, decrypt the message.

You can do this by hand. But don't: write code to do it for you.

How? Devise some method for "scoring" a piece of English plaintext. Character frequency is a good metric. Evaluate each output and choose the one with the best score.
"""

# 1. xor el string contra cada caracter del la tabla ascii
## convertir el string en int
## meter el string en un loop que xorea contra int de 1 a 127 de la tabla ascci  y guarda la salida en una lista
# 2. pasar los resultados evaluando la frecuencia de caracteres


# Tipo: [[char]] -> int /HACERLO SIEMPRE/
def max_score(char_array):
    score = 0
    max_index = 0
    max_score = 0
    for (i, a) in enumerate(char_array):
        score = char_array_score(a)
        if score > max_score:
            max_score = score
            max_index = i
    return max_index

def test_max_score():
    array_in = [["0", "0"], ["i", "E"]]
    score= max_score(array_in)
    print(score)
    assert score == 1

# Tipo: [char] -> int 
def char_array_score(char_array):
    score = 0
    for c in char_array:
        if c == 'e' or c == 'E':
            score += 21912
        elif c == 't' or c == 'T':
            score += 16587
        elif c == 'a' or c == 'A':
            score += 14810
        elif c == 'o' or c == 'O':
            score += 14003
        elif c == 'i' or c =='I':
            score += 13318
        elif c == 'n' or c == 'N':
            score += 12666
    
    return score

def test_char_arrary_score():
    test_array = ['E', '0', 'T', 'a', 'i']
#    assert char_array_score(test_array) == 7

# entra bytes sale [[byte]]
def xor_against_ascii_chars(frase_cifrada):
    """
    Function that makes XOR between:
    hex string mesaje converted to bytes
    and
    a single char converted to byte
    """
    lista_con_respuestas = []
    
    for i in range(0,255):
        respuesta = [chr(_a ^ i) for _a in frase_cifrada]
        #respuesta_string = respuesta.decode('UTF-8')
        #i_hex = hx.hex_to_base64(i_hex)
        lista_con_respuestas.append(respuesta)
    return lista_con_respuestas

def test_xor_against_ascii_chars():
    entrada = int.to_bytes(1,1,sys.byteorder)
    devolucion = xor_against_ascii_chars(entrada)
    for (i, a) in enumerate(devolucion):
        #print(a)
        #print(i ^ 1)
        assert a[0] == chr(i ^ 1)
        
        







if __name__ == "__main__":
    
    test_max_score()
    test_char_arrary_score()
    test_xor_against_ascii_chars()
    mensaje = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736'
    mensaje_bytes = hx.hex_to_bytes(mensaje) # b'\x1b77316?x\x15\x1b\x7f+x413=x9x(7-6<x7>x:9;76'
    assert len(mensaje) == 2*len(mensaje_bytes)
    lista_decodificada = xor_against_ascii_chars(mensaje_bytes)
    for i in lista_decodificada:
        print(i[:10])
    indice_max_score = max_score(lista_decodificada)
    salida_str= ""
    print(salida_str.join(lista_decodificada[indice_max_score]))
    
## Pensar todo en materia de tipos. Pensar en que tipos estoy comparando