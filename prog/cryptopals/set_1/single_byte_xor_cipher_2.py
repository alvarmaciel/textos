import hex_to_x as hx
import fixed_XOR as fx
import binascii
import sys
from binascii import unhexlify
import typing

"""
 The hex encoded string:

1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736

... has been XOR'd against a single character. Find the key, decrypt the message.

You can do this by hand. But don't: write code to do it for you.

How? Devise some method for "scoring" a piece of English plaintext. Character frequency is a good metric. Evaluate each output and choose the one with the best score.
"""

# 1. xor el string contra cada caracter del la tabla ascii
## convertir el string en int
## meter el string en un loop que xorea contra int de 1 a 127 de la tabla ascci  y guarda la salida en una lista
# 2. pasar los resultados evaluando la frecuencia de caracteres http://pi.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html


def determinar_puntaje_frase(frase: list[bytes]) -> int:
    puntaje = 0
    frecuencia_letras = {
        "e": 13.0,
        "t": 9.10,
        "a": 8.2,
        "o": 7.5,
        "i": 7.0,
        "n": 6.7,
        "s": 6.3,
        "r": 6.00,
        "h": 6.1,
        "d": 4.3,
        "l": 4.0,
        "u": 2.8,
        "c": 2.8,
        "m": 2.4,
        "f": 2.2,
        "y": 2.0,
        "w": 2.4,
        "g": 2.00,
        "p": 1.9,
        "b": 1.5,
        "v": 0.98,
        "k": 0.77,
        "x": 0.15,
        "q": 0.095,
        "j": 0.15,
        "z": 0.074,
        "`": -100,
    }
    letras_coincidentes = set(frecuencia_letras).intersection(frase)
    # puntaje = sum(frecuencia_letras[k] for k in letras_coincidentes)
    for k in letras_coincidentes:
        puntaje = puntaje + frecuencia_letras[k]
    return puntaje


def max_puntaje(posibles_respuestas: list[list[bytes]]) -> int:
    max_puntaje = 0
    puntaje_indice = int()
    for i in posibles_respuestas:
        puntaje_indice = determinar_puntaje_frase(i)
        if puntaje_indice > max_puntaje:
            max_puntaje = puntaje_indice
            indice_max_puntaje = i

    return indice_max_puntaje


# entra bytes sale [[byte]]
def xor_against_ascii_chars(frase_cifrada: list[bytes]) -> list[list[bytes]]:
    """
    Function that makes XOR between:
    hex string mesaje converted to bytes
    and
    a single char converted to byte
    """
    lista_con_respuestas = []

    for i in range(256):
        respuesta = [chr(_a ^ i) for _a in frase_cifrada]
        # respuesta_string = respuesta.decode('UTF-8')
        # i_hex = hx.hex_to_base64(i_hex)
        lista_con_respuestas.append(respuesta)
    return lista_con_respuestas


if __name__ == "__main__":
    mensaje = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
    mensaje_bytes = hx.hex_to_bytes(
        mensaje
    )  # b'\x1b77316?x\x15\x1b\x7f+x413=x9x(7-6<x7>x:9;76'
    assert len(mensaje) == 2 * len(mensaje_bytes)
    lista_decodificada = xor_against_ascii_chars(mensaje_bytes)
    indice_max_puntaje = max_puntaje(lista_decodificada)
    salida_str= ""
    print(salida_str.join(indice_max_puntaje))

## Pensar todo en materia de tipos. Pensar en que tipos estoy comparando
