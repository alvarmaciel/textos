"""This module contains a code example related to

Think Python, 2nd Edition
by Allen Downey
http://thinkpython2.com

Copyright 2015 Allen Downey

License: http://creativecommons.org/licenses/by/4.0/
"""

from __future__ import print_function, division

import math
import turtle


def square(t, length):
    """Draws a square with sides of the given length.

    Returns the Turtle to the starting position and location.
    """
    for i in range(4):
        t.fd(length)
        t.lt(90)


def polyline(t, n, length, angle):
    """Draws n line segments.

    t: Turtle object
    n: number of line segments
    length: length of each segment
    angle: degrees between segments
    """
    for i in range(n):
        t.fd(length)
        t.lt(angle)


def polygon(t, n, length):
    """Draws a polygon with n sides.

    t: Turtle
    n: number of sides
    length: length of each side.
    """
    angle = 360.0/n
    polyline(t, n, length, angle)


def arc(t, r, angle):
    """Draws an arc with the given radius and angle.

    t: Turtle
    r: radius
    angle: angle subtended by the arc, in degrees
    """
    arc_length = 2 * math.pi * r * abs(angle) / 360
    n = int(arc_length / 4) + 3
    step_length = arc_length / n
    step_angle = float(angle) / n

    # making a slight left turn before starting reduces
    # the error caused by the linear approximation of the arc
    t.lt(step_angle/2)
    polyline(t, n, step_length, step_angle)
    t.rt(step_angle/2)


def circle(t, r):
    """Draws a circle with the given radius.

    t: Turtle
    r: radius
    """
    arc(t, r, 360)

def florcita(t, r, a, p):
    """Dibuja una flor dado el radio, angulo y cantidad de pétalos
    t: tortuga
    r: radio
    a: ángulo
    p: pétalos
    """
    # for i in range(p*2):
    #     arc(t, r, 14)
    #     t.lt(360/p)
    for i in range(p):
        petalos(t, r, a)
        t.lt(360/p)

        # arc(t,r,p)
def petalos(t, r, a):
    for i in range(2):
        arc(t, r, a)
        t.lt(180-a)
def correrse(t):
    t.penup()
    t.fd(300)
    t.pendown()
def espiral (t, r, a, v):
    """Dibuja una flor dado el radio, angulo y cantidad de pétalos
        t: tortuga
        r: radio
        a: ángulo
        v: vueltas
        """
    for i in range(v):
        r = 2+a

        arc(t,r,a)


bob = turtle.Turtle()
#florcita(bob, 100, 180)
#florcita(bob, 100, 70, 7)
#correrse(bob)
#florcita(bob,200,30, 20)
#correrse(bob)
espiral(bob, 100, 90, 10)
turtle.mainloop()