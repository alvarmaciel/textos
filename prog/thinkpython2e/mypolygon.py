import turtle
import math

bob = turtle.Turtle()


def circle(t, r):
    circun = math.pi * (r * 2)
    square(t, r)
    lenght = circun / r / math.pi
    polygon(t, lenght, round(circun / 2))
    return round(circun / 2)


def square(t, length):
    length = length
    for i in range(4):
        t.fd(length)
        t.lt(90)


def polygon(t, length, n, ang):
    length = length
    ang = ang * n / 360
    for i in range(round(ang)):
        t.fd(length)
        t.lt(360 / n)


def arc(t, r, ang):
    circun = math.pi * (r * 2)  # circunferecia
    square(t, r)
    lenght = circun / r / math.pi  # calculo el largo de cada segmento
    n = round(circun / 2)
    polygon(t, lenght, n, ang)


arc(bob, r=200, ang=90)

turtle.mainloop()
