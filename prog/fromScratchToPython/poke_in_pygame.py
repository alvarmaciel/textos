import pygame
from pygame_widgets import Button
from pokebase import pokemon
import webbrowser
from requests import get
from PIL import Image
from io import BytesIO

pygame.init()
win = pygame.display.set_mode((600, 600))

button = Button(
    win,
    100,
    100,
    300,
    150,
    text="Hello",
    fontSize=50,
    margin=20,
    inactiveColour=(255, 0, 0),
    pressedColour=(0, 255, 0),
    radius=20,
    onClick=lambda: print("Click"),
)

run = True
while run:
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:
            pygame.quit()
            run = False
            quit()

    win.fill((255, 255, 255))

    button.listen(events)
    button.draw()

    pygame.display.update()

# def elegir_pokemon():
#     name = input("¿Qué pokemon querés: ")
#     print("Seleccionando a : " + name)
#     poke = pokemon(name)
#     pic = get(poke.sprites.front_default).content
#     image = Image.open(BytesIO(pic))
#     image.save("poke.gif")
#     # print("Las imágenes de " + name + " que hay son: ")
#     # for x, y in poke.sprites.__dict__.items():
#     #    print(x, y)
#     # webbrowser.open(poke.sprites.front_default)
#     # print(str(getattr(poke, "sprites")))
#     # print(name + " peso " + str(poke.weight))


# elegir_pokemon()
