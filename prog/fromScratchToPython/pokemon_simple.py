from pokebase import pokemon
import webbrowser
from requests import get
from PIL import Image
from io import BytesIO


def elegir_pokemon():
    name = input("¿Qué pokemon querés: ")
    print("Seleccionando a : " + name)
    poke = pokemon(name)
    pic = get(poke.sprites.front_default).content
    image = Image.open(BytesIO(pic))
    image.save("poke.gif")
    # print("Las imágenes de " + name + " que hay son: ")
    # for x, y in poke.sprites.__dict__.items():
    #    print(x, y)
    # webbrowser.open(poke.sprites.front_default)
    # print(str(getattr(poke, "sprites")))
    # print(name + " peso " + str(poke.weight))


elegir_pokemon()
