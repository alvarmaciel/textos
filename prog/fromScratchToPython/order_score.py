#!/usr/bin/env python3


name_and_score = "54 - Alice,35 - Bob,27 - Carol,27 - Chuck,05 - Craig,30 - Dan,27 - Erin,77 - Eve,14 - Fay,20 - Frank,48 - Grace,61 - Heidi,03 - Judy,28 - Mallory,05 - Olivia,44 - Oscar,34 - Peggy,30 - Sybil,82 - Trent,75 - Trudy,92 - Victor,37 - Walter"
# Make a lsit with the string
list_name_and_score = name_and_score.split(" - ")
print(list_name_and_score)
# Move the last index to the first (asumming Walter have 54 points)
list_name_and_score.insert(0, list_name_and_score.pop(len(list_name_and_score) - 1))
# Join the first two items of the list
list_name_and_score.insert(0, ",".join(list_name_and_score[:2]))
# Delete data repited
del list_name_and_score[1:3]

# Split elements and create new list width name and score separated
list_element_separated = []
for item in list_name_and_score:
    # Coma remove
    item_list = item.split(",")
    list_element_separated.append(item_list)
list_element_separated.sort(key=lambda x: x[1])
print("Sorted by higher number: \n ", list_element_separated)
list_element_separated.sort(key=lambda x: x[1], reverse=True)
print("Sorted by lowest number: \n ", list_element_separated)
