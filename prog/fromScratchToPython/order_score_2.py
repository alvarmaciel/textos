#!/usr/bin/env python3


name_and_score = "54 - Alice,35 - Bob,27 - Carol,27 - Chuck,05 - Craig,30 - Dan,27 - Erin,77 - Eve,14 - Fay,20 - Frank,48 - Grace,61 - Heidi,03 - Judy,28 - Mallory,05 - Olivia,44 - Oscar,34 - Peggy,30 - Sybil,82 - Trent,75 - Trudy,92 - Victor,37 - Walter"
# Make a lsit with the string
list_name_and_score = name_and_score.split(",")
list_name_and_score.sort()
print("Score ascendant: ", list_name_and_score)
list_name_and_score.sort(reverse=True)
print("Score descendent: ", list_name_and_score)
