#!/usr/bin/env python3
from random import choice, randint

adjetivos = [
    "baje",
    "guape",
    "miedose",
    "valiente",
    "calme",
    "copade",
    "buene",
    "preocupade",
    "golose",
    "jugade",
]
sustantivos = ["Gente", "Historia", "Forma", "Arte", "Mundo", "Información", "Mapa"]

primer_palabra = choice(adjetivos)
segunda_palabra = choice(sustantivos)
primer_digito = randint(0, 9)
segundo_digito = randint(0, 9)

usuario = primer_palabra + segunda_palabra + str(primer_digito) + str(segundo_digito)

print(usuario)
