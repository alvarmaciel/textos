(require 'package)
 (setq package-enable-at-startup nil)
 (add-to-list 'package-archives
 	            '("melpa" . "https://melpa.org/packages/"))

;; (add-to-list 'package-archives
;; 	     '("melpa2" . "http://www.mirrorservice.org/sites/melpa.org/packages/"))
;; (add-to-list 'package-archives
;; 	     '("melpa3" . "http://www.mirrorservice.org/sites/stable.melpa.org/packages/"))
;; ;; (add-to-list 'package-archives
;;               '("org" . "https://orgmode.org/elpa/"))
;; Use Package
(eval-when-compile
   (require 'use-package))
;; El-patch

(use-package el-patch
  :straight (:host github
                   :repo "raxod502/el-patch"
                   :branch "develop"))
(eval-when-compile
  (require 'el-patch))


;; ;; Emacs Reaveal
;; ;;; Code:
;; (set-terminal-coding-system 'utf-8)

;; ;; Enable bundled Lisp packages.
;; (add-to-list 'load-path "~/.emacs.d/elpa/emacs-reveal/org-mode/lisp")
;; (add-to-list 'load-path "~/.emacs.d/elpa/emacs-reveal/org-re-reveal")
;; (add-to-list 'load-path "~/.emacs.d/elpa/emacs-reveal/org-re-reveal-ref")
;; (add-to-list 'load-path "~/.emacs.d/elpa/emacs-reveal/oer-reveal")

;; ;; Enable custom packages.
;; (package-initialize)

;; (add-to-list 'load-path "~/.emacs.d/elpa/emacs-reveal")
;; (require 'emacs-reveal)
;; (require 'org-re-reveal)
;; (package-initialize)
;; (setq
;;  oer-reveal-plugins
;;  '("reveal.js-plugins" "Reveal.js-TOC-Progress" "reveal.js-jump-plugin"
;;    "reveal.js-quiz" "reveal.js-coursemod" "klipse-libs"))

;; Bootstrap `use-package'

 (unless (package-installed-p 'use-package)
 	(package-refresh-contents)
 	(package-install 'use-package))

;; Straight
 (defvar bootstrap-version)
 (let ((bootstrap-file
        (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
       (bootstrap-version 5))
   (unless (file-exists-p bootstrap-file)
     (with-current-buffer
         (url-retrieve-synchronously
          "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
          'silent 'inhibit-cookies)
       (goto-char (point-max))
       (eval-print-last-sexp)))
   (load bootstrap-file nil 'nomessage))


  ;;______________________________________________________________________
  ;;;;  Installing Org with straight.el
  ;;; https://github.com/raxod502/straight.el/blob/develop/README.md#installing-org-with-straightel
  (require 'subr-x)
  (straight-use-package 'git)

  (defun org-git-version ()
    "The Git version of 'org-mode'.
  Inserted by installing 'org-mode' or when a release is made."
    (require 'git)
    (let ((git-repo (expand-file-name
                     "straight/repos/org/" user-emacs-directory)))
      (string-trim
       (git-run "describe"
                "--match=release\*"
                "--abbrev=6"
                "HEAD"))))

  (defun org-release ()
    "The release version of 'org-mode'.
  Inserted by installing 'org-mode' or when a release is made."
    (require 'git)
    (let ((git-repo (expand-file-name
                     "straight/repos/org/" user-emacs-directory)))
      (string-trim
       (string-remove-prefix
        "release_"
        (git-run "describe"
                 "--match=release\*"
                 "--abbrev=0"
                 "HEAD")))))

  (provide 'org-version)

  ;; (straight-use-package 'org) ; or org-plus-contrib if desired

  (use-package org-plus-contrib
     :mode (("\\.org$" . org-mode)))
  ;-  :ensure org-plus-contrib



;; Org Roam
(use-package org-roam
  :hook 
  (after-init . org-roam-mode)
  :commands (org-roam-build-cache)
  :straight (:host github :repo "jethrokuan/org-roam")
  :custom
  (org-roam-directory "~/Dropbox/org/pensadero/org/")
  :bind (:map org-roam-mode-map
            (("C-c n l" . org-roam)
              ("C-c n f" . org-roam-find-file)
              ("C-c n b" . org-roam-switch-to-buffer)
              ("C-c n g" . org-roam-show-graph))
              :map org-mode-map
              (("C-c n i" . org-roam-insert))))

;:config
;(require 'org-roam-protocol)
;(setq org-roam-capture-templates
;      '(("d" "default" plain (function org-roam--capture-get-point)
;         "%?"
;     :file-name "%<%Y%m%d%H%M%S>-${slug}"
;     :head "#+TITLE: ${title}\n"
;     :unnarrowed t))
;      ("p" "private" plain (function org-roam--capture-get-point)
;           "%?"
;           :file-name "private-${slug}"
;           :head "#+TITLE: ${title}\n"
;           :unnarrowed t)))


;; DEFT
;; (require 'deft)
;; (setq deft-directory "~/Dropbox/org/pensadero/org")
;; (setq deft-extension "org")
;; (setq deft-text-mode 'org-mode)
;; (setq deft-use-filename-as-title t)
;; (setq deft-auto-save-interval 0)

(use-package deft
  ;:disabled t
  :after org
  :bind
  ("C-c n d" . deft)
  :custom
(deft-recursive t)
  (deft-use-filter-string-for-filename t)
  (deft-default-extension "org")
  (deft-text-mode 'org-mode)
  (deft-directory "~/Dropbox/org/pensadero/org")
   :config/el-patch
   (defun deft-parse-title (file contents)
;;     "Parse the given FILE and CONTENTS and determine the title.
;; If `deft-use-filename-as-title' is nil, the title is taken to
;; be the first non-empty line of the FILE.  Else the base name of the FILE is
;; used as title."
     (el-patch-swap (if deft-use-filename-as-title
                        (deft-base-filename file)
                      (let ((begin (string-match "^.+$" contents)))
                        (if begin
                            (funcall deft-parse-title-function
                                     (substring contents begin (match-end 0))))))
                    (org-roam--get-title-or-slug file))))


(org-babel-load-file (expand-file-name "~/.emacs.d/myinit.org"))





(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(browse-url-browser-function 'browse-url-firefox)
 '(org-agenda-start-on-weekday 0)
 '(org-babel-load-languages
   '((emacs-lisp . t)
     (C . t)
     (ruby . t)
     (shell . t)
     (python . t)
     (org . t)
     (js . t)))
 '(org-default-notes-file (concat org-directory "/notes.org"))
 '(org-directory "~/Dropbox/orgfiles")
 '(org-export-html-postamble nil)
 '(org-hide-leading-stars t)
 '(org-list-allow-alphabetical t)
 '(org-mind-map-engine "dot")
 '(org-modules
   '(ol-bbdb ol-bibtex org-crypt org-ctags ol-docview ol-eww ol-gnus org-habit org-id ol-info org-inlinetask org-mouse org-protocol ol-w3m ol-eshell org-bbdb org-crypt org-docview org-gnus org-habit org-info org-habit org-irc org-mouse org-protocol org-annotate-file org-eval org-expiry org-interactive-query ol-man org-collector org-panel org-screen org-toc))
 '(org-re-reveal-extra-attr nil)
 '(org-re-reveal-plugins '(markdown highlight zoom notes search multiplex))
 '(org-roam-directory "~/Dropbox/org/pensadero/org/")
 '(org-startup-folded 'overview)
 '(org-startup-indented t)
 '(org-support-shift-select nil)
 '(org-trello-current-prefix-keybinding "C-c o" nil (org-trello))
 '(org-trello-files '("~/Dropobx/orgfiles/sadosky-trello.org") nil (org-trello))
 '(org-use-property-inheritance nil)
 '(package-selected-packages
   '(nimbus-theme bongo org-emms emms plantuml-mode org-plus-contrib all-the-icons org-trello treemacs-magit treemacs-icons-dired treemacs-projectile treemacs-evil treemacs ox-hugo ggtags org-babel ox-reveal magit NeoTree neotree projectile org-mind-map flycheck-rtags company-rtags helm-rtags rtags irony-eldoc flycheck-irony company-irony irony nov nav org-gcal org-pdfview pdf-tools web-mode iedit move-text hungry-delete beacon jekyll-modes markdown-mode undo-tree elpy flycheck zenburn-theme counsel ace-window noflet org-ac org-bullets which-key try use-package))
 '(python-shell-virtualenv-root "/usr/local/lib/python3.6"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:inherit ace-jump-face-foreground :height 3.0)))))
;;(put 'narrow-to-region 'disabled nil)
;;(put 'narrow-to-page 'disabled nil)
